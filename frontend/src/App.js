import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'; 
import ImageStorage from './ImageStroage';
import CameraRealTime from './components/CameraRealTime';
import Header from './components/Header';
import Dashboard from './components/Dashboard';


function App() {
  return (
    <Router>
      
      <div className="d-flex flex-column min-vh-100">
        <Header/>
        <Routes className="flex-grow-1">
          <Route path="/" element={<Dashboard />} />
          <Route path="/camera-real-time" element={<CameraRealTime />} />
          <Route path="/image-storage" element ={<ImageStorage/>}/>
        </Routes>
        <footer className="bg-dark text-light text-center p-3 fixed-bottom">
          <p>&copy; 2023 Crowdy. All rights reserved.</p>
        </footer>
      </div>
    </Router>
  );
}

export default App;
