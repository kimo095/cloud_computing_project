import React, { useEffect, useState } from "react";

const TotalHeadCount = ({ cameraData }) => {
  const [totalHeadCount, setTotalHeadCount] = useState(0);

  useEffect(() => {
    // Calculate total head count from all cameras
    const totalCount = cameraData.reduce(
      (accumulator, camera) => accumulator + camera.latestHeadCount,
      0
    );
    setTotalHeadCount(totalCount);
  }, [cameraData]);

  return (
    <div>
      <h3>Total Head Count</h3>
      <h4>{totalHeadCount}</h4>
    </div>
  );
};

export default TotalHeadCount;
