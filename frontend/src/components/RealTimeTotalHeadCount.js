import React, { useState, useEffect } from 'react';
import axios from 'axios';

const RealTimeTotalHeadCount = () => {
  const [totalHeadCount, setTotalHeadCount] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('/api/real-time-total-head-count');
        setTotalHeadCount(response.data.totalHeadCount);
      } catch (error) {
        console.error('Error fetching real-time total head count:', error);
      }
    };

    fetchData();

    // Fetch data every 30 seconds
    const intervalId = setInterval(fetchData, 30000);

    // Clear interval on component unmount
    return () => clearInterval(intervalId);
  }, []);

  return (
    <div className="container mt-4">
      <h2 className="mb-4">Real-Time Total Head Count</h2>
      <p>Total Head Count: {totalHeadCount}</p>
    </div>
  );
};

export default RealTimeTotalHeadCount;
