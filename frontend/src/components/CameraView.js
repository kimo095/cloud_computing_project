import React, { useState } from "react";
import styled from "styled-components";

const CameraContainer = styled.div`
  margin: 10px;
`;

const CameraImage = styled.img`
  width: 200px;
  height: 150px;
  object-fit: cover;
  cursor: pointer;
`;

const CameraView = ({ cameraData, onSelectImage }) => {
  const { cameraName, latestPhoto, latestHeadCount } = cameraData;

  // State to track the previous photo for comparison
  const [previousPhoto, setPreviousPhoto] = useState(null);

  const handleSelectImage = () => {
    onSelectImage(latestPhoto);
  };

  // Function to determine if the current photo is sufficiently different
  const isDifferentPhoto = () => {
    return previousPhoto !== latestPhoto;
  };

  // Update the previous photo if it's sufficiently different
  if (isDifferentPhoto()) {
    setPreviousPhoto(latestPhoto);
  }

  return (
    <CameraContainer>
      <p>{cameraName}</p>
      <CameraImage src={latestPhoto} alt={cameraName} onClick={handleSelectImage} />
      <p>Latest Head Count: {latestHeadCount}</p>
    </CameraContainer>
  );
};

export default CameraView;
