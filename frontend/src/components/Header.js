import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

function Header() {
  return (
    <>
      <Navbar style={{ backgroundColor: 'lightblue' }} variant="dark">
        <div className="container d-flex flex-row justify-content-between align-items-center">
          {/* Logo */}
          <Navbar.Brand href="#home">
            <img
              src='/crowdylogotextblack.svg'
              alt="Crowdy Logo"
              style={{ width: '17%' }}
            />
          </Navbar.Brand>

          {/* Navigation Links */}
          <Nav className="ml-auto">
          <Nav.Link href="/" style={{ fontWeight: 'bold', color: 'black', marginRight: '15px' }}>Dashboard</Nav.Link>
          <Nav.Link href="/image-storage" style={{ fontWeight: 'bold', color: 'black', marginRight: '15px' }}>ImageStorage</Nav.Link>
            <Nav.Link
              href="/camera-real-time"
              style={{
                fontWeight: 'bold',
                color: 'black',
                marginRight: '15px',
                whiteSpace: 'nowrap',
              }}
            >
              Live Camera
            </Nav.Link>
          </Nav>
        </div>
      </Navbar>
    </>
  );
}

export default Header;
