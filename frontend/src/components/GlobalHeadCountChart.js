import React, { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import axios from 'axios';
import 'chart.js/auto';

const GlobalHeadCountChart = () => {
  const [headCountData, setHeadCountData] = useState({
    labels: [],
    datasets: [
      {
        label: 'Head Count',
        data: [],
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 1,
      },
    ],
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('/api/global-head-count');
        setHeadCountData(response.data);
      } catch (error) {
        console.error('Error fetching global head count data:', error);
      }
    };

    fetchData();

    // Fetch data every 30 seconds
    const intervalId = setInterval(fetchData, 30000);

    // Clear interval on component unmount
    return () => clearInterval(intervalId);
  }, []);

  return (
    <div className="container mt-4"> 
      <h2 className="mb-4">Global Head Count</h2> 
      <Bar data={headCountData} options={{}} />
    </div>
  );
};

export default GlobalHeadCountChart;
