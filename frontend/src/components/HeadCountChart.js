// components/HeadCountChart.js

import React, { useEffect, useRef } from 'react';
import Chart from 'chart.js/auto';

const HeadCountChart = ({ headCounts = [] }) => {
  const chartRef = useRef(null);

  useEffect(() => {
    if (chartRef.current) {
      const ctx = chartRef.current.getContext('2d');
      const chart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: Array.from({ length: headCounts.length }, (_, i) => i),
          datasets: [{
            label: 'Head Count',
            data: headCounts,
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            borderColor: 'rgba(75, 192, 192, 1)',
            borderWidth: 2
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      });

      return () => {
        chart.destroy();
      };
    }
  }, [headCounts]);

  return <canvas ref={chartRef} />;
};

export default HeadCountChart;
