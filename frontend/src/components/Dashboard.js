import React, { useRef, useState, useEffect } from "react";
import styled from "styled-components";
import { useDropzone } from "react-dropzone";
import "@tensorflow/tfjs-backend-cpu";
import * as cocoSsd from "@tensorflow-models/coco-ssd";
import HeadCountChart from "./HeadCountChart";
import CameraView from "./CameraView";
import TotalHeadCount from "./TotalHeadCount";

const FullPageContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  min-height: 100vh;
  padding-bottom: 60px;
`;

const ObjectDetectorContainer = styled.div`
  flex: 2;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  justify-content: space-between;
`;

const DetectorContainer = styled.div`
  width: 80%;
  max-width: 800px;
  height: 80%;
  max-height: 600px;
  border: 3px solid #fff;
  border-radius: 5px;
  position: relative;
  margin-top: 20px;
`;

const TargetImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 5px;
`;

const DragDropContainer = styled.div`
  display: ${({ imgUploaded }) => (imgUploaded ? "none" : "flex")};
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border: 2px dashed #ccc;
  border-radius: 5px;
  cursor: pointer;
  margin-top: 20px;
  width: 300%;
  background-color: #f8f8f8;
`;

const HeadCountChartWrapper = styled.div`
  margin-right: 10px; /* Adjust the margin as needed */
`;

const ResultContainer = styled.div`
  flex: 1;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 900px;
`;

const HiddenFileInput = styled.input`
  display: none;
`;

const TargetBox = styled.div`
  position: absolute;
  left: ${({ x }) => x + "px"};
  top: ${({ y }) => y + "px"};
  width: ${({ width }) => width + "px"};
  height: ${({ height }) => height + "px"};
  border: 4px solid #1ac71a;
  background-color: transparent;
  z-index: 20;

  &::before {
    content: "${({ classType, score }) => `${classType} ${score.toFixed(1)}%`}";
    color: #1ac71a;
    font-weight: 500;
    font-size: 14px;
    position: absolute;
    top: -1.5em;
    left: -5px;
  }
`;

const TotalCountContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

const Footer = styled.footer`
  position: fixed;
  bottom: 0;
  width: 100%;
  background-color: #f8f8f8;
  padding: 15px;
  text-align: center;
`;

export function Dashboard (props) {
  const fileInputRef = useRef();
  const imageRef = useRef();
  const [imgData, setImgData] = useState(null);
  const [predictions, setPredictions] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [personCount, setPersonCount] = useState(0);
  const [headCounts, setHeadCounts] = useState([]);
  const [selectedCamera, setSelectedCamera] = useState(null);

  const cameraData = [
    // will be Replace with actual camera data
    {
      cameraName: "Camera 1",
      latestPhoto: imgData,
      latestHeadCount: personCount,
    },
    {
      cameraName: "Camera 2",
      latestPhoto: "path/to/latest/photo2.jpg",
      latestHeadCount: 5,
    },
    // Add more cameras as needed
  ];

  const handleImageSelectionForCamera = (camera) => {
    setSelectedCamera(camera);
    openFilePicker();
  };

  const renderCameraViews = () => {
    return (
      <div style={{ display: "flex", justifyContent: "center", marginTop: "20px" }}>
        {cameraData.map((camera, idx) => (
          <CameraView
            key={idx}
            cameraData={camera}
            onSelectImage={() => handleImageSelectionForCamera(camera)}
          />
        ))}
      </div>
    );
  };

  useEffect(() => {
    const intervalId = setInterval(() => {
      setHeadCounts((prevHeadCounts) => {
        const updatedHeadCounts = [...prevHeadCounts, personCount].slice(-30);
        return updatedHeadCounts;
      });
    }, 1000);

    return () => {
      clearInterval(intervalId);
    };
  }, [personCount]);

  const isEmptyPredictions = !predictions || predictions.length === 0;

  const onDrop = (acceptedFiles) => {
    if (acceptedFiles && acceptedFiles.length > 0) {
      handleImageSelection(acceptedFiles[0]);
    }
  };

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: "image/*",
  });

  const openFilePicker = () => {
    if (fileInputRef.current) fileInputRef.current.click();
  };

  const handleImageSelection = async (file) => {
    setPredictions([]);
    setLoading(true);

    const imgData = await readImage(file);
    setImgData(imgData);

    const imageElement = document.createElement("img");
    imageElement.src = imgData;

    imageElement.onload = async () => {
      const imgSize = {
        width: imageElement.width,
        height: imageElement.height,
      };
      await detectObjectsOnImage(imageElement, imgSize);
      setLoading(false);
    };
  };

  const normalizePredictions = (predictions, imgSize) => {
    if (!predictions || !imgSize || !imageRef) return predictions || [];
    return predictions.map((prediction) => {
      const { bbox } = prediction;
      const oldX = bbox[0];
      const oldY = bbox[1];
      const oldWidth = bbox[2];
      const oldHeight = bbox[3];

      const imgWidth = imageRef.current.width;
      const imgHeight = imageRef.current.height;

      const x = (oldX * imgWidth) / imgSize.width;
      const y = (oldY * imgHeight) / imgSize.height;
      const width = (oldWidth * imgWidth) / imgSize.width;
      const height = (oldHeight * imgHeight) / imgSize.height;

      return { ...prediction, bbox: [x, y, width, height] };
    });
  };

  const detectObjectsOnImage = async (imageElement, imgSize) => {
    const model = await cocoSsd.load({});
    const predictions = await model.detect(imageElement, 6);
    const normalizedPredictions = normalizePredictions(predictions, imgSize);
    setPredictions(normalizedPredictions);

    const personPredictions = normalizedPredictions.filter(
      (prediction) => prediction.class === "person"
    );
    setPersonCount(personPredictions.length);
  };

  const readImage = (file) => {
    return new Promise((rs, rj) => {
      const fileReader = new FileReader();
      fileReader.onload = () => rs(fileReader.result);
      fileReader.onerror = () => rj(fileReader.error);
      fileReader.readAsDataURL(file);
    });
  };

  return (
    <FullPageContainer>
      <ObjectDetectorContainer>
        <TotalCountContainer>
          {personCount > 0 && <p>Total persons detected: {personCount}</p>}
        </TotalCountContainer>
        <DragDropContainer imgUploaded={imgData !== null} {...getRootProps()}>
          <input {...getInputProps()} />
          <p>Drag & drop an image here, or click to select one</p>
        </DragDropContainer>
        <DetectorContainer>
          {imgData && <TargetImg src={imgData} ref={imageRef} />}
          {!isEmptyPredictions &&
            predictions.map((prediction, idx) => (
              <TargetBox
                key={idx}
                x={prediction.bbox[0]}
                y={prediction.bbox[1]}
                width={prediction.bbox[2]}
                height={prediction.bbox[3]}
                classType={prediction.class}
                score={prediction.score * 100}
              />
            ))}
        </DetectorContainer>
        <HiddenFileInput
          type="file"
          ref={fileInputRef}
          onChange={(e) => {
            handleImageSelection(e.target.files[0]);
            setSelectedCamera(null);
          }}
        />
        {isLoading && <p>Recognizing...</p>}
        {selectedCamera && (
          <div>
            <p>Selected Camera: {selectedCamera.cameraName}</p>
            <div {...getInputProps()} />
          </div>
        )}
          <HeadCountChart headCounts={headCounts} />
       
        </ObjectDetectorContainer>
        <ResultContainer>
  <HeadCountChartWrapper>
  {renderCameraViews()}
  </HeadCountChartWrapper>
  <TotalHeadCount cameraData={cameraData} />
</ResultContainer>
      <Footer>
      </Footer>
    </FullPageContainer>
  );
}
export default Dashboard