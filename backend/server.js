const express = require('express');
const cors = require('cors');
const { json } = require('body-parser');
const axios = require('axios');
const cloudinary = require('cloudinary').v2;

const app = express();

app.use(cors());
app.use(json());

const BASE_URL = 'https://api.cloudinary.com/v1_1/diorkmvfc';
const auth = {
  api_key: "291194326242397",
  api_secret: "pMoTb3YHZMx4bHIU0Pr6vOJxDG8",
};

cloudinary.config(auth);

// Axios request/response logging
axios.interceptors.request.use(request => {
  console.log('Starting Request', request);
  return request;
});

axios.interceptors.response.use(response => {
  console.log('Response:', response.data);
  return response;
});

app.get('/photos', async (req, res) => {
  try {
    const response = await axios.get(BASE_URL + '/resources/image', {
      params: {
        next_cursor: req.query.next_cursor,
      },
      headers: {
        Authorization: `Basic ${Buffer.from(`${auth.api_key}:${auth.api_secret}`).toString('base64')}`,
      },
    });
    return res.send(response.data);
  } catch (error) {
    console.error('Error fetching photos:', error.response ? error.response.data : error.message);
    return res.status(error.response ? error.response.status : 500).send(error.response ? error.response.data : 'Internal Server Error');
  }
});

app.get('/search', async (req, res) => {
  try {
    const response = await axios.get(BASE_URL + '/resources/search', {
      params: {
        expression: req.query.expression,
      },
      headers: {
        Authorization: `Basic ${Buffer.from(`${auth.api_key}:${auth.api_secret}`).toString('base64')}`,
      },
    });
    return res.send(response.data);
  } catch (error) {
    console.error('Error searching:', error.response ? error.response.data : error.message);
    return res.status(error.response ? error.response.status : 500).send(error.response ? error.response.data : 'Internal Server Error');
  }
});

const PORT = 7000;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
